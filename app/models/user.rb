class User < ApplicationRecord
    validates :username, uniqueness: true, presence: true
    validates_presence_of :token
    validates_presence_of :role
  
    enum role: ['default', 'admin']
end
